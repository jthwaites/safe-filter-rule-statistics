const fs = require('fs');
const path = require('path');

const folderPath = './ruleSets';

let overallSafe = 0;
let overallUnsafe = 0;
let overallRedirect = 0;
let overallModifyingHeaders = 0;

const tableData = [];

function isRuleSafe(rule) {
    const safeActions = ["block", "allow", "allowAllRequests", "upgradeScheme"];
    return safeActions.includes(rule.action.type);
}

fs.readdir(folderPath, (err, files) => {
    if (err) {
        console.error("Could not list the directory.", err);
        process.exit(1);
    }

    files.forEach((file, index) => {
        const unsafeRulesInFile = [];
        const unsafeTypes = {};
        let totalUnsafe = 0;
        let totalSafe = 0;
        let rulesInFile = 0;

        const filePath = path.join(folderPath, file);

        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error(`Error reading file ${file}`, err);
                return
            }

            const jsonData = JSON.parse(data);
            for (let rule of jsonData) {
                rulesInFile++;

                if (isRuleSafe(rule)) {
                    totalSafe++;
                } else {
                    totalUnsafe++;
                    unsafeRulesInFile.push(rule);

                    if (unsafeTypes[rule.action.type]) {
                        unsafeTypes[rule.action.type]++;
                    } else {
                        unsafeTypes[rule.action.type] = 1;
                    }
                }
            }

            overallSafe += totalSafe;
            overallUnsafe += totalUnsafe;

            if (unsafeTypes["redirect"]) {
                overallRedirect += unsafeTypes["redirect"];
            }

            if (unsafeTypes["modifyHeaders"]) {
                overallModifyingHeaders += unsafeTypes["modifyHeaders"];
            }

            const totalRules = overallSafe + overallUnsafe;
            let totalSafePercentage = (overallSafe / totalRules) * 100;
            totalSafePercentage = Math.round(totalSafePercentage * 100) / 100
            let totalUnsafePercentage = (overallUnsafe / totalRules) * 100;
            totalUnsafePercentage = Math.round(totalUnsafePercentage * 100) / 100
            console.log("total safe percentage " + totalSafePercentage);
            console.log("total unsafe percentage " + totalUnsafePercentage);


            let safePercentageInFile = (totalSafe / rulesInFile) * 100;
            safePercentageInFile = Math.round(safePercentageInFile * 100) / 100
            const modifyHeaders = unsafeTypes["modifyHeaders"] ? unsafeTypes["modifyHeaders"] : 0;
            const redirect = unsafeTypes["redirect"] ? unsafeTypes["redirect"] : 0;

            const fileName = file.substring(0, 6);

            const fileData = { fileName, rulesInFile, "safe %": safePercentageInFile, safe: totalSafe, unsafe: totalUnsafe, redirect, modifyHeaders };
            tableData.push(fileData);
            console.table(tableData)
        });
    });
});