# Safe/Unsafe Rule Data

A script to get the stats on safe and unsafe rules from the [webext-sdk repository](https://gitlab.com/eyeo/adblockplus/abc/webext-sdk/).

The proposed definition of a "safe" rule is:

* The action is "block", "allow", "allowAllRequests" or "upgradeScheme"
* The action is "redirect" and the "extensionPath" property is used rather than "url",
  AND the HTTP method is "GET" (this is intended to cover redirecting to a mock JS file etc.
  without allowing intercepting data).

## Running Locally

```
    node getData.js 
```

## Generating the rulesets

The rulesets were generated from the webext-sdk repository use the following commands:

```
    npm exec subs-init
```

```
    npm exec subs-fetch
```

```
    npm exec subs-convert
```

The generated rulesets directory was then copied from webext-sdk to this repo and the .map files were deleted.
